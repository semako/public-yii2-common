<?php

namespace semako\yii2Common\interfaces;

/**
 * Interface IManage
 * @package semako\yii2Common\interfaces
 */
interface IManage
{
    /**
     * @return IResultBool
     */
    public function run();
}
