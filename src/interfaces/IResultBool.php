<?php

namespace semako\yii2Common\interfaces;

/**
 * Interface IResultBool
 * @package semako\yii2Common\interfaces
 */
interface IResultBool
{
    /**
     * Get result execute
     * @return bool
     */
    public function result();

    /**
     * Get notification text
     * @return string
     */
    public function reason();
}
