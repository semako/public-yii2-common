<?php

namespace semako\yii2Common\interfaces;

/**
 * Interface IActiveRecord
 * @package semako\yii2Common\interfaces
 */
interface IActiveRecord extends INullObject
{
    /**
     * @return int
     */
    public function getPk();

    /**
     * @return int
     */
    public function getCreatedAt();

    /**
     * @return null|int
     */
    public function getUpdatedAt();
}
