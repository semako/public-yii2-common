<?php

namespace semako\yii2Common\interfaces;

/**
 * Interface INullObject
 * @package semako\yii2Common\interfaces
 */
interface INullObject
{
    /**
     * @return bool
     */
    public function isNullObject();
}
