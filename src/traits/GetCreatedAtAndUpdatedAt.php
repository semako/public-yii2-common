<?php

namespace semako\yii2Common\traits;

use semako\yii2Common\components\ActiveRecord;

/**
 * Class GetCreatedAtAndUpdatedAt
 * @package semako\yii2Common\traits
 */
trait GetCreatedAtAndUpdatedAt
{
    /**
     * @return int
     */
    public function getCreatedAt()
    {
        /* @var ActiveRecord $this */
        return $this->getAttribute(ActiveRecord::$fieldCreated);
    }

    /**
     * @return int|null
     */
    public function getUpdatedAt()
    {
        /* @var ActiveRecord $this */
        return $this->getAttribute(ActiveRecord::$fieldUpdated);
    }
}
