<?php

namespace semako\yii2Common\traits;
use semako\vkApi\interfaces\common\IToArray;

/**
 * Class ToArray
 * @package semako\yii2Common\traits
 */
trait ToArray
{
    /**
     * @return string
     */
    public function toArray()
    {
        /** @var IToArray|int|string|bool $item */
        /** @var IToArray|int|string|bool $value */

        $result = [];
        $methods = get_class_methods($this);

        foreach ($methods as $method) {
            if (!preg_match('/^get(.+)$/is', $method, $match)) {
                continue;
            }

            $name = strtolower(preg_replace('/^_/is', '', preg_replace('/([A-Z]+)/s', '_$1', $match[1])));
            $item = $this->$method();

            if (is_array($item)) {
                $result[$name] = [];
                foreach ($item as $value) {
                    $result[$name][] = is_object($value) ? $value->toArray() : $value;
                }
            } else {
                $result[$name] = is_object($item) ? $item->toArray() : $item;
            }
        }

        return $result;
    }
}
