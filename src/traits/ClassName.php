<?php

namespace semako\yii2Common\traits;

/**
 * Class ClassName
 * @package semako\yii2Common\traits
 */
trait ClassName
{
    /**
     * @return string
     */
    public static function className()
    {
        return get_called_class();
    }
}
