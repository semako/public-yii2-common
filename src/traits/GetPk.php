<?php

namespace semako\yii2Common\traits;

use semako\yii2Common\components\ActiveRecord;

/**
 * Class GetPk
 * @package semako\yii2Common\traits
 */
trait GetPk
{
    /**
     * @return int
     */
    public function getPk()
    {
        /* @var ActiveRecord $this */
        return $this->getAttribute(ActiveRecord::$fieldPk);
    }
}
