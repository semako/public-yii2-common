<?php

namespace semako\yii2Common\traits;

use semako\yii2Common\components\ActiveQuery;
use semako\yii2Common\components\ActiveRecord;

/**
 * Class FindByPk
 * @package semako\yii2Common\traits
 */
trait FindByPk
{
    /**
     * @param int $pk
     * @return $this
     */
    public function byPk($pk)
    {
        /* @var ActiveQuery $this */
        return $this->andWhere('[[' . ActiveRecord::$fieldPk . ']] = :id', [
            ':id' => $pk,
        ]);
    }
}
