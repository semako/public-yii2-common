<?php

namespace semako\yii2Common\components;

use semako\yii2Common\interfaces\IResultBool;

/**
 * Class ResultBool
 * @package semako\yii2Common\components
 */
class ResultBool implements IResultBool
{
    /**
     * Notification text
     * @var string
     */
    private $reason;

    /**
     * Result execute
     * @var bool
     */
    private $result;

    /**
     * @param bool $result
     * @param string|string[] $reason
     */
    public function __construct($result, $reason = '')
    {
        $this->setResult($result);
        $this->setReason($reason);
    }

    /**
     * Factory true result.
     * @param string $reason
     * @return static
     */
    public static function true($reason = '')
    {
        return new static(true, $reason);
    }

    /**
     * Factory false result.
     * @param string|string[] $reason
     * @return static
     */
    public static function false($reason = '')
    {
        return new static(false, $reason);
    }

    /**
     * Set notification text
     * @param string|string[] $reason
     */
    public function setReason($reason)
    {
        $this->reason = $this->prepareReason($reason);
    }

    /**
     * Prepare notification text
     * @param string|string[] $reason
     * @return string
     */
    private function prepareReason($reason)
    {
        if (is_string($reason) || is_int($reason)) {
            return $reason;
        }

        if ($reason instanceof ActiveRecord) {
            return $reason->getAttributes();
        }

        if (!is_array($reason)) {
            return '';
        }

        $result = [];

        foreach ($reason as $reasonOne) {
            $result[] = $this->prepareReason($reasonOne);
        }

        return trim(implode(PHP_EOL, $result));
    }

    /**
     * Get notification text
     * @return string
     */
    public function reason()
    {
        return $this->reason;
    }

    /**
     * Set result execute
     * @param boolean $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * Get result execute
     * @return bool
     */
    public function result()
    {
        return (bool)$this->result;
    }
}
