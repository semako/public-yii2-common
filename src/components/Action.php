<?php

namespace semako\yii2Common\components;

use yii\base\Action as BaseAction;

/**
 * Class Action
 * @package semako\yii2Common\components
 */
class Action extends BaseAction
{
    /**
     * @return \yii\base\Controller|\yii\web\Controller
     */
    protected function getController()
    {
        return $this->controller;
    }
}
