<?php

namespace semako\yii2Common\components;

use semako\yii2Common\traits\FindByPk;
use yii\db\ActiveQuery as BaseActiveQuery;

/**
 * Class ActiveQuery
 * @package semako\yii2Common\components
 */
class ActiveQuery extends BaseActiveQuery
{
    use FindByPk;

    /**
     * @inheritdoc
     * @return ActiveQuery[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ActiveQuery|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
