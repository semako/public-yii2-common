<?php

namespace semako\yii2Common\components;

use semako\yii2Common\interfaces\IActiveRecord;
use semako\yii2Common\traits\GetCreatedAtAndUpdatedAt;
use semako\yii2Common\traits\GetPk;
use yii\db\ActiveRecord as BaseActiveRecord;

/**
 * Class ActiveRecord
 * @package semako\yii2Common\components
 */
class ActiveRecord extends BaseActiveRecord implements IActiveRecord
{
    use GetPk;
    use GetCreatedAtAndUpdatedAt;

    public static $fieldPk      = 'id';
    public static $fieldCreated = 'created_at';
    public static $fieldUpdated = 'updated_at';

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->hasAttribute(self::$fieldCreated) && $this->getIsNewRecord()) {
            $this->setAttribute(self::$fieldCreated, time());
        }

        if ($this->hasAttribute(self::$fieldUpdated) && !$this->getIsNewRecord()) {
            $this->setAttribute(self::$fieldUpdated, time());
        }

        return parent::beforeValidate();
    }

    /**
     * @return bool
     */
    public function isNullObject()
    {
        return false;
    }
}
